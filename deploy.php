<?php
	/**
	 * GIT DEPLOYMENT SCRIPT
	 *
	 * Used for automatically deploying websites via github or bitbucket, more deets here:
	 *
	 *		https://gist.github.com/1809044
	 */
	set_time_limit(500);
    
    $KEY = 'YrF8k779gh3l1npct';
	$CLIENT = '3TP';
	$PROJECT = 'master';
	$CHANNEL = 'deploybot';
	
    if ($_GET['k'] != $KEY)
    {
        die('Wrong verification');
    }
	
	$commands = array(
		'echo $PWD',
		'whoami',
		'git stash',
        'git fetch',
		'git reset --hard origin/master',
		'git submodule sync',
		'git submodule update',
		'git submodule status',
	);

	// Run the commands for output
	$output = '';
	foreach($commands AS $command){
		// Run it
		$tmp = shell_exec($command);
		// Output
		$output .= '$ '.$command."\r\n";
		$output .= $tmp."\r\n";
	}

	echo $output;
	
	$tmp = shell_exec('git diff | wc -l');
	if ($tmp > 0)
	{
		$git_diff = shell_exec('git diff --name-only');
		//slack('A commit has been pushed to '.$CLIENT.' - '.$PROJECT.' that was auto-deployed. Local changes were noted. Files include: '."\r\n".$git_diff, $CHANNEL);
	}
    
    $git_sl = shell_exec('git stash list');
    if (trim($git_sl) != '')
    {
		//slack('A commit has been pushed to '.$CLIENT.' - '.$PROJECT.' that was auto-deployed. Local stash changes were noted. This should be investigated manually.', $CHANNEL);
    }
	
	function slack($message, $room = "team-dev", $icon = ":loudspeaker:") {
        return;
    }
?>