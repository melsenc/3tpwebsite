const PDFDocument = require('pdfkit');

function buildPDF(dataCallback, endCallback) {
  // Defaults for all layouts
  let doc_width = 1296;
  let doc_height = 1620;
  let location_center = doc_width/2;
  let location_left = location_center - 288;
  let location_right = location_center + 288;
  let template_width_14in = 1008;
  let template_width_11in = 792;
  let template_width_5in = 360;
  let image_y_origin_normal = 162;
  let image_y_origin_high = 126;

  // Specific settings for this Version
  let template_width = template_width_14in;
  let template_center = location_center;
  let image_y_origin = image_y_origin_normal;
  let image_height = 792;
  let image_rotated = false;

  // Calculate placement and rotation
  let template_x_origin = template_center - template_width/2;
  let image_rotation = image_rotated ? 180 : 0;

  const doc = new PDFDocument({
    layout: 'portrait',
    size: [doc_width,doc_height],
    margins: {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    }
  });
  doc.on('data', dataCallback);
  doc.on('end', endCallback);

  doc.rect(0,0,doc_width,doc_height).fill('#FFFFFF'); // fill the page with a background rectangle, to keep the size constant

  doc.font('Helvetica-Bold')
    .fontSize(12)
    .fill('#000000')
    .text(
      'DTS Layout',
      360,
      64.8,
      {width: 576, align: 'center'}
    );
  
  doc.rotate(image_rotation, {origin: [template_width/2+template_x_origin, image_height/2+image_y_origin]})
  .image(
    'images/seps/MIFF112A01.png',
    template_x_origin, // distance from left edge to the start of the design area
    image_y_origin, // distance down from top
    {fit: [template_width, image_height], align: 'center', valign: 'center'} // the width is the area within which the design will be centered, the height needs to be set from the database, the width is the 
  )

  // Center Top registration mark
  doc.moveTo(doc_width/2, image_y_origin-14.4)
    .lineTo(doc_width/2, image_y_origin-40.68)
    .stroke();

  doc.moveTo(doc_width/2-13.14, image_y_origin-27.54)
    .lineTo(doc_width/2+13.14, image_y_origin-27.54)
    .stroke();

  doc.circle(doc_width/2, image_y_origin-27.54, 7.25)
    .stroke();

  // Center Bottom registration mark
  doc.moveTo(doc_width/2, image_y_origin+14.4+image_height)
    .lineTo(doc_width/2, image_y_origin+40.68+image_height)
    .stroke();

  doc.moveTo(doc_width/2-13.14, image_y_origin+27.54+image_height)
    .lineTo(doc_width/2+13.14, image_y_origin+27.54+image_height)
    .stroke();

  doc.circle(doc_width/2, image_y_origin+27.54+image_height, 7.25)
    .stroke();

  // Left registration mark
  doc.moveTo(doc_width/2-template_width/2-27.54, image_y_origin+image_height/2-13.14)
    .lineTo(doc_width/2-template_width/2-27.54, image_y_origin+image_height/2+13.14)
    .stroke();

  doc.moveTo(doc_width/2-template_width/2-14.4, image_y_origin+image_height/2)
    .lineTo(doc_width/2-template_width/2-40.68, image_y_origin+image_height/2)
    .stroke();

  doc.circle(doc_width/2-template_width/2-27.54, image_y_origin+image_height/2, 7.25)
    .stroke();

  // Right registration mark
  doc.moveTo(doc_width/2+template_width/2+27.54, image_y_origin+image_height/2-13.14)
    .lineTo(doc_width/2+template_width/2+27.54, image_y_origin+image_height/2+13.14)
    .stroke();

  doc.moveTo(doc_width/2+template_width/2+14.4, image_y_origin+image_height/2)
    .lineTo(doc_width/2+template_width/2+40.68, image_y_origin+image_height/2)
    .stroke();

  doc.circle(doc_width/2+template_width/2+27.54, image_y_origin+image_height/2, 7.25)
    .stroke();

  doc.end();
}

module.exports = { buildPDF };

