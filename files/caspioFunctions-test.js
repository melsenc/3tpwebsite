/* This is a function that causes a window to reload if the focus has gone away from the window. 
From https://stackoverflow.com/questions/11313434/how-to-refresh-page-after-focus
and also this: https://stackoverflow.com/questions/55127650/location-reloadtrue-is-deprecated
I'm commenting out here because it is a function we will only want to use in some cases.
I'll leave in this file for reference, but will implement individually on certain pages.*/

// window.onblur= function() {window.onfocus= function () {location.reload()}};

// Below are some functions for dealing with custom buttons in Caspio:

function changeDrop(dropdown, option) {
    if (dropdown.selectedIndex) {
        dropdown.children[dropdown.selectedIndex].removeAttribute("selected", "selected");
    }
    dropdown.children[option].setAttribute("selected", "selected");
}

function deleteRecord(formIndex, category, statusFieldName, activityFieldNameAsString, activityIndex, destDropAsString, destIndex) {
    var f = document.forms[formIndex];
    var question = "Are you sure you want to delete this " + category + " record?";

    if (confirm(question)) {
        document.getElementsByName(("EditRecord" + statusFieldName))[0].checked = false;
        changeDrop(document.getElementById(activityFieldNameAsString), activityIndex);
        changeDrop(document.getElementsByName(destDropAsString)[0], destIndex);
        f.submit();
    } else {
        return false;
    }
}

function checkStatusBilled(statusValue, callBackFunction, recordType) {
    var question = "You are about to modify a BILLED RECORD. Are you sure you want to edit this " + recordType + "?";

    switch (statusValue) {
        case 3:
            if (confirm(question)) {
                callBackFunction();
            } 
            break;
        default:
            callBackFunction();
    }
}

function makeButton(idAsString, functionAsString, buttonText, typeAsString, referenceButton) {
    // Check to see if it already exists
    var existButton = document.getElementById(idAsString);

    if (existButton === null) {
        // Create the new button
        var myButton = document.createElement('button');
        myButton.setAttribute("onclick", functionAsString);
        myButton.setAttribute("class", "cbButton");
        myButton.innerHTML = buttonText;
        myButton.setAttribute("id", idAsString);
        myButton.setAttribute("type", typeAsString);

        // Make the button float left
        myButton.style.cssFloat = "left";

        // Insert the new button as the last sibling of the referenceButton
        referenceButton.parentNode.appendChild(myButton);
    }
}

// This is a function for displaying images and documents throughout the website:

function displayFile(containerDivId, fileName_field, s3URL_field, width, height, classname = 'display-file') { // classname is optional, and will be given the default value of display-file if not specified
    // Load the div ID as a variable
    var containerDiv = document.getElementById(containerDivId);
    // Only display the file if the container div is empty
    if (containerDiv && !containerDiv.childElementCount) {
        var url;
        if (fileName_field) {
            url = fileName_field;
        }
        else if (s3URL_field) {
            url = 'https://' + s3URL_field;
        } else { }

        /* When files are this type: {.doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .pages, .ai, .psd, .tiff, .dxf, .eps, .ps, .ttf, .xps, .zip, .rar}, then use google docs viewer embedded iframe to show them.
        Otherwise, show them using the browser native display
        (Note: I want to use the browser for svg files, which is why I'm not including it in this list, but google docs view can handle them, too) */

        if (url) {

            let ending = url.split('.').pop().toLowerCase();
            let findExtension = (extension) => {
                if (ending.includes(extension)) {
                    return ending;
                }
            };
            
            switch (ending) {
                case findExtension('doc'):
                case findExtension('docx'):
                case findExtension('xls'):
                case findExtension('xlsx'):
                case findExtension('ppt'):
                case findExtension('pptx'):
                case findExtension('pdf'):
                case findExtension('pages'):
                case findExtension('ai'):
                case findExtension('psd'):
                case findExtension('tiff'):
                case findExtension('dxf'):
                case findExtension('eps'):
                case findExtension('ps'):
                case findExtension('ttf'):
                case findExtension('xps'):
                case findExtension('zip'):
                case findExtension('rar'):
                
                    $(containerDiv).append("<iframe src='https://docs.google.com/gview?url=" + (url.split('https://').pop()) + "&embedded=true' style='width:" + width + "px; height:" + height + "px;' frameborder='0'></iframe>");

                    break;

                default:

                    // code to display browser compatible file types
                    $(containerDiv).append("<img id='img-" + containerDivId + "' src='" + url + "' style='max-width:" + width + "px; max-height:" + height + "px; border-radius: 3px; cursor: pointer; transition: 0.3s;' onmouseover = 'this.style.opacity = \"0.7\";' onmouseout = 'this.style.opacity = \"1\";' class='" + classname + "' />");

                    // build the modal for browser compatible file types
                    var $modal = $("<div id='modal-" + containerDivId + "' class='modal'><img class='modal-content' id='imgModal-" + containerDivId + "'><br /><br /><br /><br /><br /><br /><br /><br /></div>");

                    // insert the modal after the container div
                    $(containerDiv).after($modal);

                    // Get the image and insert it inside the modal
                    var sourceImg = document.getElementById("img-" + containerDivId);
                    var modalImg = document.getElementById("imgModal-" + containerDivId);
                    sourceImg.onclick = function () {
                        $modal.css('display', 'block');
                        modalImg.src = this.src;
                    }

                    // When the user clicks on the modal, close the modal
                    modalImg.onclick = function () {
                        $modal.css('display', 'none');
                    }
            }

        } else { }
    }
}

// https://stackoverflow.com/questions/35969656/how-can-i-generate-the-opposite-color-according-to-current-color

function invertColor(hex) {
    if (hex) {
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        // convert 3-digit hex to 6-digits.
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        var r = parseInt(hex.slice(0, 2), 16),
            g = parseInt(hex.slice(2, 4), 16),
            b = parseInt(hex.slice(4, 6), 16);
            // http://stackoverflow.com/a/3943023/112731
            return (r * 0.299 + g * 0.587 + b * 0.114) > 186
                ? '#000000'
                : '#FFFFFF';
    }
}

/* The solution found here: http://www.aspsnippets.com/Articles/Change-URL-in-Browser-Address-Bar-without-reloading-using-JavaScript-and-jQuery.aspx */

/** HTML5 History pushState method
 * The pushState method works similar to window.location but it does not refresh or reload the page and it will modify the URL even if the page does not exists. The pushState method actually inserts an entry into the history of the browsers which allows us to go back and forth using the browser’s forward and back buttons.
 * The pushState method accepts the following three parameters.
 * 1. State object: - The state object is a JavaScript object which can contain any details about the page and must be serializable.
 * 2. Title: - The title of the page.
 * 3. URL – The URL of the page.
 */

/** Using JavaScript */
function ChangeUrl(title, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Title: title, Url: url };
        history.pushState(obj, obj.Title, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

/** Follow this format below to write specific jQuery functions to specific datapages:
$(function () {
	$("#button1").click(function () {
		ChangeUrl('Page1', 'Page1.htm');
	});
	$("#button2").click(function () {
		ChangeUrl('Page2', 'Page2.htm');
	});
	$("#button3").click(function () {
		ChangeUrl('Page3', 'Page3.htm');
	});
});*/