/* This is a function that causes a window to reload if the focus has gone away from the window. 
From https://stackoverflow.com/questions/11313434/how-to-refresh-page-after-focus
and also this: https://stackoverflow.com/questions/55127650/location-reloadtrue-is-deprecated
I'm commenting out here because it is a function we will only want to use in some cases.
I'll leave in this file for reference, but will implement individually on certain pages.*/

// window.onblur= function() {window.onfocus= function () {location.reload()}};

// Below are some functions for dealing with custom buttons in Caspio:

function changeDrop(dropdown, option) {
    if (dropdown.selectedIndex) {
        dropdown.children[dropdown.selectedIndex].removeAttribute("selected", "selected");
    }
    dropdown.children[option].setAttribute("selected", "selected");
}

function deleteRecord(formIndex, category, statusFieldName, activityFieldNameAsString, activityIndex, destDropAsString, destIndex) {
    var f = document.forms[formIndex];
    var question = "Are you sure you want to delete this " + category + " record?";

    if (confirm(question)) {
        document.getElementsByName(("EditRecord" + statusFieldName))[0].checked = false;
        changeDrop(document.getElementById(activityFieldNameAsString), activityIndex);
        changeDrop(document.getElementsByName(destDropAsString)[0], destIndex);
        f.submit();
    } else {
        return false;
    }
}

function checkStatusBilled(statusValue, callBackFunction, recordType) {
    var question = "You are about to modify a BILLED RECORD. Are you sure you want to edit this " + recordType + "?";

    switch (statusValue) {
        case 3:
            if (confirm(question)) {
                callBackFunction();
            } 
            break;
        default:
            callBackFunction();
    }
}

function makeButton(idAsString, functionAsString, buttonText, typeAsString, referenceButton) {
    // Check to see if it already exists
    var existButton = document.getElementById(idAsString);

    if (existButton === null) {
        // Create the new button
        var myButton = document.createElement('button');
        myButton.setAttribute("onclick", functionAsString);
        myButton.setAttribute("class", "cbButton");
        myButton.innerHTML = buttonText;
        myButton.setAttribute("id", idAsString);
        myButton.setAttribute("type", typeAsString);

        // Make the button float left
        myButton.style.cssFloat = "left";

        // Insert the new button as the last sibling of the referenceButton
        referenceButton.parentNode.appendChild(myButton);
    }
}

// This is a function for displaying images and documents throughout the website:

function displayFile(containerDivId, fileName_field, s3URL_field, width, height, classname = 'display-file') { // classname is optional, and will be given the default value of display-file if not specified
    // Load the div ID as a variable
    var containerDiv = document.getElementById(containerDivId);
    // Only display the file if the container div is empty
    if (containerDiv && !containerDiv.childElementCount) {
        var url;
        if (fileName_field) {
            url = fileName_field;
        }
        else if (s3URL_field) {
            url = 'https://' + s3URL_field;
        } else { }

        /* When files are this type: {.doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .pages, .ai, .psd, .tiff, .dxf, .eps, .ps, .ttf, .xps, .zip, .rar}, then use google docs viewer embedded iframe to show them.
        Otherwise, show them using the browser native display
        (Note: I want to use the browser for svg files, which is why I'm not including it in this list, but google docs view can handle them, too) */

        if (url) {

            let ending = url.split('.').pop().toLowerCase();
            let findExtension = (extension) => {
                if (ending.includes(extension)) {
                    return ending;
                }
            };
            
            switch (ending) {
                case findExtension('doc'):
                case findExtension('docx'):
                case findExtension('xls'):
                case findExtension('xlsx'):
                case findExtension('ppt'):
                case findExtension('pptx'):
                case findExtension('pdf'):
                case findExtension('pages'):
                case findExtension('ai'):
                case findExtension('psd'):
                case findExtension('tiff'):
                case findExtension('dxf'):
                case findExtension('eps'):
                case findExtension('ps'):
                case findExtension('ttf'):
                case findExtension('xps'):
                case findExtension('zip'):
                case findExtension('rar'):
                
                    $(containerDiv).append("<iframe src='https://docs.google.com/gview?url=" + (url.split('https://').pop()) + "&embedded=true' style='width:" + width + "px; height:" + height + "px;' frameborder='0'></iframe>");

                    break;

                default:

                    // code to display browser compatible file types
                    $(containerDiv).append("<img id='img-" + containerDivId + "' src='" + url + "' style='max-width:" + width + "px; max-height:" + height + "px; border-radius: 3px; cursor: pointer; transition: 0.3s;' onmouseover = 'this.style.opacity = \"0.7\";' onmouseout = 'this.style.opacity = \"1\";' class='" + classname + "' />");

                    // build the modal for browser compatible file types
                    var $modal = $("<div id='modal-" + containerDivId + "' class='modal'><img class='modal-content' id='imgModal-" + containerDivId + "'><br /><br /><br /><br /><br /><br /><br /><br /></div>");

                    // insert the modal after the container div
                    $(containerDiv).after($modal);

                    // Get the image and insert it inside the modal
                    var sourceImg = document.getElementById("img-" + containerDivId);
                    var modalImg = document.getElementById("imgModal-" + containerDivId);
                    sourceImg.onclick = function () {
                        $modal.css('display', 'block');
                        modalImg.src = this.src;
                    }

                    // When the user clicks on the modal, close the modal
                    modalImg.onclick = function () {
                        $modal.css('display', 'none');
                    }
            }

        } else { }
    }
}

// I'm breaking out the URL functionality so I can use it for the QR Code Generator, to link directly to files (so people don't have to log in, and can use their phones to view color images from Order Jobs Summary page print-outs)

function getURL(fileName_field, s3URL_field) {
    var url;
        if (fileName_field) {
            // Needs to be an external link because people will be linking from their phones
            url = 'https://cdn.caspio.com/1DAE6000' + fileName_field.replace('amp;','');
        }
        else if (s3URL_field) {
            url = 'https://' + s3URL_field;
        } else { }
    return url;
}

// This is a new version of displayFile that is for responsive pages using bootstrap

function displayFileResp(containerDivId, fileName_field, s3URL_field, width, height, classname = 'display-file img-fluid') { // classname is optional, and will be given the default value of display-file if not specified
    // Load the div ID as a variable
    var containerDiv = document.getElementById(containerDivId);
    // Only display the file if the container div is empty
    if (containerDiv && !containerDiv.childElementCount) {
        var url;
        if (fileName_field) {
            url = fileName_field;
        }
        else if (s3URL_field) {
            url = 'https://' + s3URL_field;
        } else { }

        /* When files are this type: {.doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .pages, .ai, .psd, .tiff, .dxf, .eps, .ps, .ttf, .xps, .zip, .rar}, then use google docs viewer embedded iframe to show them.
        Otherwise, show them using the browser native display
        (Note: I want to use the browser for svg files, which is why I'm not including it in this list, but google docs view can handle them, too) */

        if (url) {

            let ending = url.split('.').pop().toLowerCase();
            let findExtension = (extension) => {
                if (ending.includes(extension)) {
                    return ending;
                }
            };
            
            switch (ending) {
                case findExtension('doc'):
                case findExtension('docx'):
                case findExtension('xls'):
                case findExtension('xlsx'):
                case findExtension('ppt'):
                case findExtension('pptx'):
                case findExtension('pdf'):
                case findExtension('pages'):
                case findExtension('ai'):
                case findExtension('psd'):
                case findExtension('tiff'):
                case findExtension('dxf'):
                case findExtension('eps'):
                case findExtension('ps'):
                case findExtension('ttf'):
                case findExtension('xps'):
                case findExtension('zip'):
                case findExtension('rar'):
                
                    $(containerDiv).append("<iframe src='https://docs.google.com/gview?url=" + (url.split('https://').pop()) + "&embedded=true' style='width:" + width + "px; height:" + height + "px;' frameborder='0'></iframe>");

                    break;

                default:

                    // code to display browser compatible file types
                    $(containerDiv).append("<img id='img-" + containerDivId + "' src='" + url + "' style='max-width: " + width + "px; max-height: " + height + "px; border-radius: 3px; cursor: pointer; transition: 0.3s;' onmouseover = 'this.style.opacity = \"0.7\";' onmouseout = 'this.style.opacity = \"1\";' class='" + classname + "' />");

                    // build the modal for browser compatible file types
                    var $modal = $("<div id='modal-" + containerDivId + "' class='container-fluid modal'><div class='row' style='height:100%'><div class='col d-flex align-items-center justify-content-center'><img class='modal-content img-fluid my-auto' id='imgModal-" + containerDivId + "'><br /><br /><br /><br /><br /><br /><br /><br /></div></div></div>");

                    // insert the modal at the end of the body element
                    $('body').append($modal);

                    // Get the image and insert it inside the modal
                    var sourceImg = document.getElementById("img-" + containerDivId);
                    var modalImg = document.getElementById("imgModal-" + containerDivId);
                    sourceImg.onclick = function () {
                        $modal.css('display', 'block');
                        modalImg.src = this.src;
                    }

                    // When the user clicks on the modal, close the modal
                    var containerModal = document.getElementById("modal-" + containerDivId);
                    containerModal.onclick = function () {
                        $modal.css('display', 'none');
                    }
            }

        } else { }
    }
}

// https://stackoverflow.com/questions/35969656/how-can-i-generate-the-opposite-color-according-to-current-color

function invertColor(hex) {
    if (hex) {
        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        // convert 3-digit hex to 6-digits.
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        var r = parseInt(hex.slice(0, 2), 16),
            g = parseInt(hex.slice(2, 4), 16),
            b = parseInt(hex.slice(4, 6), 16);
            // http://stackoverflow.com/a/3943023/112731
            return (r * 0.299 + g * 0.587 + b * 0.114) > 186
                ? '#000000'
                : '#FFFFFF';
    }
}

/* The solution found here: http://www.aspsnippets.com/Articles/Change-URL-in-Browser-Address-Bar-without-reloading-using-JavaScript-and-jQuery.aspx */

/** HTML5 History pushState method
 * The pushState method works similar to window.location but it does not refresh or reload the page and it will modify the URL even if the page does not exists. The pushState method actually inserts an entry into the history of the browsers which allows us to go back and forth using the browser’s forward and back buttons.
 * The pushState method accepts the following three parameters.
 * 1. State object: - The state object is a JavaScript object which can contain any details about the page and must be serializable.
 * 2. Title: - The title of the page.
 * 3. URL – The URL of the page.
 */

/** Using JavaScript */
function ChangeUrl(title, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = { Title: title, Url: url };
        history.pushState(obj, obj.Title, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

/** Follow this format below to write specific jQuery functions to specific datapages:
$(function () {
	$("#button1").click(function () {
		ChangeUrl('Page1', 'Page1.htm');
	});
	$("#button2").click(function () {
		ChangeUrl('Page2', 'Page2.htm');
	});
	$("#button3").click(function () {
		ChangeUrl('Page3', 'Page3.htm');
	});
});*/

// Functions for using reset buttons, based on the above format, tailored to our needs
    function clearFields() {
        var dropdowns = $('select');
        var i = 0;
        while (dropdowns[i]) {
            dropdowns[i].selectedIndex = 0;
            i++;
        }
        var textboxes = $('input[type="text"]');
        var j = 0;
        while (textboxes[j]) {
            $(textboxes[j]).val('');
            j++;
        }
    }

    function resetSearch(pageTitle, cbHostPage) {
        clearFields();
        ChangeUrl(pageTitle + ' - Three Twins Productions, Inc', cbHostPage);
    }

/** Code from Caspio Devs for deploying datapages in a modal, accessed via button */
var cbAccountId = 'c0dca200';
// var cbAppKeyPrefix = '1dae6000';

var cbDomain = 'https://' + cbAccountId + '.caspio.com';
var cbDataPagePrefix = cbDomain + '/dp/'/** + cbAppKeyPrefix*/;

// function - deploy DP in modal
function openModal(modalTitle, appKey, params)
{
	$('#cb-modal-body').html('');
	deployDP('cb-modal-body', appKey, params);

	$('#cb-modal-title').html(modalTitle);

	// draggable modal
	$(".modal-header").on("mousedown", function(mousedownEvt) {
		var $draggable = $(this);
		var x = mousedownEvt.pageX - $draggable.offset().left,
		y = mousedownEvt.pageY - $draggable.offset().top;

		$("body").on("mousemove.draggable", function(mousemoveEvt) {
			$draggable.closest(".modal-content").offset({
				"left": mousemoveEvt.pageX - x,
				"top": mousemoveEvt.pageY - y
			});
		});

		$("body").one("mouseup", function() {
			$("body").off("mousemove.draggable");
		});

		$draggable.closest(".modal").one("bs.modal.hide", function() {
			$("body").off("mousemove.draggable");
		});
	});
}

function deployDP(containerID, appKey, params)
{
	var params = params || '';
	var dataPageScript = document.createElement("script");
	var container = document.getElementById(containerID);
	dataPageScript.src = cbDataPagePrefix + appKey + '/emb' + params;

	container.innerHTML = '';
	container.appendChild(dataPageScript);
}

function toggleDisplayBlockNone(id) {
    var x = document.getElementById(id);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

function toggleDisplayBlock(id) {
    var x = document.getElementById(id);
    x.style.display = "block";
}

function toggleDisplayNone(id) {
    var x = document.getElementById(id);
    x.style.display = "none";
}

// This visibility function is from https://jsfiddle.net/elmarj/u35tez5n/5/ and the article https://stackoverflow.com/questions/1462138/event-listener-for-when-element-becomes-visible
  toggleVisibility = function(targetId) {
    const myElement = document.getElementById(targetId);
    myElement.classList.toggle("hidden");
  }

// Write the loading-mask div to the body section
var loadingMask = document.createElement('div');
loadingMask.setAttribute("class", "container-fluid");
loadingMask.setAttribute("style", "display: none;");
loadingMask.innerHTML = '<div class="row" style="height:100%;"><div class="col d-flex align-items-center justify-content-center"><img src="files/spinner.svg" alt="spinner loading icon" class="img-fluid my-auto"></div></div>';
loadingMask.setAttribute("id", "loading-mask");
document.body.appendChild(loadingMask);

function navButtonSpinner() {
    toggleDisplayBlock("loading-mask");
}

function navButtonSpinnerHide() {
    toggleDisplayNone("loading-mask");
}

function enableSpinner() {
    // When a form is submitted, show the spinner
    document.addEventListener('BeforeFormSubmit', function(event) {
        navButtonSpinner();
    });

    // When a DataPageReady event occurs, hide the spinner
    document.addEventListener('DataPageReady', function(event) {
        navButtonSpinnerHide();
    });
}

// this is a slight variation on enableSpinner() that allows you to wait for a specific datapage to be ready, for multi-datapage layouts
function enableSpinnerDatapageKey(datapageAppKey) {
    // When a form is submitted, show the spinner
    document.addEventListener('BeforeFormSubmit', function(event) {
        navButtonSpinner();
    });

    // When a DataPageReady event occurs for the specific datapage, hide the spinner
    document.addEventListener('DataPageReady', function(event) {
        if (event.detail.appKey == datapageAppKey) {
            navButtonSpinnerHide();
        }
    });
}

// In order for this function to work properly, you need to add the .hide-child class to any elements you want to watch. If all elements are display:none, then the parent will be marked as display:none
function hideParent(parentID) {
    $('#' + parentID ).show();
    if($('#' + parentID + ' .hide-child').length > 0 && ($('#' + parentID + ' .hide-child').length === $('#' + parentID + ' .hide-child:hidden').length)){
        $('#' + parentID ).hide();
    }
}

// This function is supposed to be the opposite of hideParent(). In order for it to do anything, the parentID elements must start out as display:none.
function showParent(parentID) {
    if($('#' + parentID + ' .hide-child').length > 0 && ($('#' + parentID + ' .hide-child').length > $('#' + parentID + ' .hide-child:hidden').length)){
        $('#' + parentID ).show();
    }
}

/* Forked from https://codepen.io/sakamies/pen/yzYypW */

    /* Example on how to use this at the bottom here. Implementation first. */

    function LoadingSpinner (form, spinnerHTML) {
        form = form || document;
    
        //Keep track of button & spinner, so there's only one automatic spinner per form
        var button;
        var spinner = document.createElement('div');
        spinner.innerHTML = spinnerHTML;
        spinner = spinner.firstChild;
    
        //Delegate events to a root element, so you don't need to attach a spinner to each individual button.
        form.addEventListener('click', start);
    
        //Stop automatic spinner if validation prevents submitting the form
        //Invalid event doesn't bubble, so use capture
        form.addEventListener('invalid', stop, true);
    
        //Start spinning only when you click a submit button
        function start (event) {
            if (button) {stop();}
                button = event.target;
            if (button.type === 'submit') {
                LoadingSpinner.start(button, spinner);
            }
        }
    
        function stop () {
            LoadingSpinner.stop(button, spinner);
        }
    
        function destroy () {
            stop();
            form.removeEventListener('click', start);
            form.removeEventListener('invalid', stop, true);
        }
    
        return {start: start, stop: stop, destroy: destroy};
    }
  
    //I guess these would be called class methods. They're useable without instantiating a new LoadingSpinner so you can start & stop spinners manually on any elements, for ajax and stuff.
    LoadingSpinner.start = function (element, spinner) {
        element.classList.add('loading');
        return document.body.appendChild(spinner);
    }
    
    LoadingSpinner.stop = function (element, spinner) {
        element.classList.remove('loading');
        return spinner.remove();
    }
    
    //Example on how to use LoadingSpinner
    
    //Bring your own spinner. You can use any html as the spinner. You can find lots of cool spinners for example here on Codepen. I'm using just plain text. Maybe technically not a spinner, but this is more about the script than graphics.
    //var loadingSpinnerHTML = '<div id="loading-mask" class="container-fluid"><div class="row" style="height:100%;"><div id="spinner" class="col d-flex align-items-center justify-content-center"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;display:block;" width="60px" height="60px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" alt="spinner loading icon" class="img-fluid my-auto"><g transform="translate(78,50)"><g transform="rotate(0)"><circle cx="0" cy="0" r="6" fill="#522994" fill-opacity="1"><animateTransform attributeName="transform" type="scale" begin="-1.75s" values="1.6600000000000001 1.6600000000000001;1 1" keyTimes="0;1" dur="2s" repeatCount="indefinite"></animateTransform><animate attributeName="fill-opacity" keyTimes="0;1" dur="2s" repeatCount="indefinite" values="1;0" begin="-1.75s"></animate></circle></g></g><g transform="translate(69.79898987322333,69.79898987322332)"><g transform="rotate(45)"><circle cx="0" cy="0" r="6" fill="#522994" fill-opacity="0.875"><animateTransform attributeName="transform" type="scale" begin="-1.5s" values="1.6600000000000001 1.6600000000000001;1 1" keyTimes="0;1" dur="2s" repeatCount="indefinite"></animateTransform><animate attributeName="fill-opacity" keyTimes="0;1" dur="2s" repeatCount="indefinite" values="1;0" begin="-1.5s"></animate></circle></g></g><g transform="translate(50,78)"><g transform="rotate(90)"><circle cx="0" cy="0" r="6" fill="#522994" fill-opacity="0.75"><animateTransform attributeName="transform" type="scale" begin="-1.25s" values="1.6600000000000001 1.6600000000000001;1 1" keyTimes="0;1" dur="2s" repeatCount="indefinite"></animateTransform><animate attributeName="fill-opacity" keyTimes="0;1" dur="2s" repeatCount="indefinite" values="1;0" begin="-1.25s"></animate></circle></g></g><g transform="translate(30.201010126776673,69.79898987322333)"><g transform="rotate(135)"><circle cx="0" cy="0" r="6" fill="#522994" fill-opacity="0.625"><animateTransform attributeName="transform" type="scale" begin="-1s" values="1.6600000000000001 1.6600000000000001;1 1" keyTimes="0;1" dur="2s" repeatCount="indefinite"></animateTransform><animate attributeName="fill-opacity" keyTimes="0;1" dur="2s" repeatCount="indefinite" values="1;0" begin="-1s"></animate></circle></g></g><g transform="translate(22,50)"><g transform="rotate(180)"><circle cx="0" cy="0" r="6" fill="#522994" fill-opacity="0.5"><animateTransform attributeName="transform" type="scale" begin="-0.75s" values="1.6600000000000001 1.6600000000000001;1 1" keyTimes="0;1" dur="2s" repeatCount="indefinite"></animateTransform><animate attributeName="fill-opacity" keyTimes="0;1" dur="2s" repeatCount="indefinite" values="1;0" begin="-0.75s"></animate></circle></g></g><g transform="translate(30.201010126776666,30.201010126776673)"><g transform="rotate(225)"><circle cx="0" cy="0" r="6" fill="#522994" fill-opacity="0.375"><animateTransform attributeName="transform" type="scale" begin="-0.5s" values="1.6600000000000001 1.6600000000000001;1 1" keyTimes="0;1" dur="2s" repeatCount="indefinite"></animateTransform><animate attributeName="fill-opacity" keyTimes="0;1" dur="2s" repeatCount="indefinite" values="1;0" begin="-0.5s"></animate></circle></g></g><g transform="translate(49.99999999999999,22)"><g transform="rotate(270)"><circle cx="0" cy="0" r="6" fill="#522994" fill-opacity="0.25"><animateTransform attributeName="transform" type="scale" begin="-0.25s" values="1.6600000000000001 1.6600000000000001;1 1" keyTimes="0;1" dur="2s" repeatCount="indefinite"></animateTransform><animate attributeName="fill-opacity" keyTimes="0;1" dur="2s" repeatCount="indefinite" values="1;0" begin="-0.25s"></animate></circle></g></g><g transform="translate(69.79898987322332,30.201010126776666)"><g transform="rotate(315)"><circle cx="0" cy="0" r="6" fill="#522994" fill-opacity="0.125"><animateTransform attributeName="transform" type="scale" begin="0s" values="1.6600000000000001 1.6600000000000001;1 1" keyTimes="0;1" dur="2s" repeatCount="indefinite"></animateTransform><animate attributeName="fill-opacity" keyTimes="0;1" dur="2s" repeatCount="indefinite" values="1;0" begin="0s"></animate></circle></g></g></svg></div></div></div>';
    //var exampleForm = document.querySelector('#example');
    //var exampleLoader = new LoadingSpinner(exampleForm, loadingSpinnerHTML);
    //Delay submit so you can see the spinner spinning, then stop the loading spinner instead of submitting because we're on Codepen.
    /*exampleForm.addEventListener('submit', function (event) {
        event.preventDefault()
        setTimeout(function () {
        exampleLoader.stop()
        }, 2000)
    });
    */