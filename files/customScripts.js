/* This script is from https://css-tricks.com/scaled-proportional-blocks-with-css-and-javascript/

It is a script for proportionally resizing contents of a container
*/

var i, j;
for (i = 0; i < $(".scaleable-wrapper").length; i++) {
    for (j = 0; j < $(".very-specific-design").length; i++) {

        var $wrapper = $(".scaleable-wrapper")[i];

        var $el = $(".very-specific-design")[j];
        var elHeight = $el.outerHeight();
        var elWidth = $el.outerWidth();

        $wrapper.resizable({
            resize: doResize
        });

        function doResize(event, ui) {

            var scale, origin;

            scale = Math.min(
                ui.size.width / elWidth,
                ui.size.height / elHeight
            );

            $el.css({
                transform: "translate(-50%, -50%) " + "scale(" + scale + ")"
            });

        }

        var starterData = {
            size: {
                width: $wrapper.width(),
                height: $wrapper.height()
            }
        }
        doResize(null, starterData);
    }
}