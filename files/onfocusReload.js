/* This is a function that causes a window to reload if the focus has gone away from the window. 
From https://stackoverflow.com/questions/11313434/how-to-refresh-page-after-focus
and also this: https://stackoverflow.com/questions/55127650/location-reloadtrue-is-deprecated */

window.onblur= function() {window.onfocus= function () {location.reload()}};